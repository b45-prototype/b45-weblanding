<!-- Jumbotron start -->
<section>
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Get work done
                <span>faster
                </span><br>
                and
                <span>better
                </span>with us
            </h1>
            <a href="" class="btn btn-primary tombol">Our Word</a>
        </div>
    </div>
</section>
<!-- Jusmbotron end -->

<!-- Container start -->
<div class="container">
    <!-- Panel info -->
    <div class="row justify-content-center">
        <div class="col-10 panel-info">
            <div class="row">
                <div class="col-lg">
                    <img src="<?= base_url();?>assets/img/employee.png" class="float-left" alt="">
                    <h4>24 Hours</h4>
                    <p>Lorem ipsum dolor sit amet.</p>
                </div>
                <div class="col-lg">
                    <img src="<?= base_url();?>assets/img/hires.png" class="float-left" alt="">
                    <h4>High Res</h4>
                    <p>Lorem ipsum dolor sit amet.</p>
                </div>
                <div class="col-lg">
                    <img src="<?= base_url();?>assets/img/security.png" class="float-left" alt="">
                    <h4>Security</h4>
                    <p>Lorem ipsum dolor sit amet.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Workingspace -->
    <div class="row workingspace">
        <div class="col-lg">
            <img
                src="<?= base_url();?>/assets/img/workingspace.png"
                alt=""
                class="img-fluid">
        </div>
        <div class="col-lg">
            <h3>You
                <span>work
                </span>like at
                <span>home</span></h3>
            <p>Bekerja dengan suasana hati yang lebih asik dan mempelajari hal baru setiap
                harinya.</p>
            <a href="" class="btn btn-primary tombol">Gallery</a>
        </div>
    </div>

    <!-- Testimonial -->
    <section class="testimonial">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <h5>" Bekerja dengan suasana hati yang lebih asik dan mempelajari hal baru "</h5>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg justify-content-center d-flex">
                <figure class="figure">
                    <img src="<?= base_url();?>/assets/img/img1.png" class="figure-img img-fluid rounded-circle" alt="Testi 1">
                </figure>
                <figure class="figure">
                    <img src="<?= base_url();?>/assets/img/img2.png" class="figure-img img-fluid rounded-circle utama" alt="Testi 2">
                    <figcaption class="figure-caption">
                    <h5>Sunny Ye</h5>
                    <p>Designer</p>
                    </figcaption>
                </figure>
                <figure class="figure">
                    <img src="<?= base_url();?>/assets/img/img3.png" class="figure-img img-fluid rounded-circle" alt="Testi 3">
                </figure>
            </div>
        </div>
    </section>

</div>
<!-- Container end -->