        <!-- Footer -->
        <div class="row footer">
            <div class="col text-center">
                <p> 2019 All Rights Reserdev By B45</p>
            </div>
        </div>
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script
            src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
            crossorigin="anonymous"></script>
        <script
            src="<?= base_url() ;?>assets/js/bootstrap.min.js"></script>
        <script 
            src="<?= base_url();?>assets/dist/sweetalert2.all.min.js"></script>
        <script 
            src="<?= base_url();?>assets/js/script.js"></script>
    </body>
</html>