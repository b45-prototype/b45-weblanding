<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?= base_url() ;?>assets/css/bootstrap.min.css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Viga" rel="stylesheet"> 

        <!-- My CSS -->
        <link rel="stylesheet" href="<?= base_url();?>assets/css/style.css">

        <title><?= $title; ?></title>
    </head>
    <body>
        <!-- Navbar start -->
        <section>
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="#">B45corp</a>
                    <button
                        class="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarNavAltMarkup"
                        aria-controls="navbarNavAltMarkup"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav ml-auto">
                            <a class="nav-item nav-link active" href="#">Home
                                <span class="sr-only">(current)</span>
                            </a>
                            <a class="nav-item nav-link" href="#">Pricing</a>
                            <a class="nav-item nav-link" href="#">Features</a>
                            <a class="nav-item nav-link" href="#">About</a>
                            <a class="nav-item btn btn-primary tombol" href="#">Join Us</a>
                        </div>
                    </div>
                </nav>
            </div>
        </section>
        <!-- Navbar end -->